
function createNewUser() {
    const newUser = {
        firstName: '',
        lastName: '',
        getLogin: function () {
            const firstNameInitial = this.firstName.charAt(0).toLowerCase();
            const login = firstNameInitial + this.lastName.toLowerCase();
            return login;
        },
        setFirstName: function (newFirstName) {
            this.firstName = newFirstName;
        },
        setLastName: function (newLastName) {
            this.lastName = newLastName;
        }
    };

    newUser.firstName = prompt('Введіть ім\'я:');
    newUser.lastName = prompt('Введіть прізвище:');

    return newUser;
}

const user = createNewUser();
console.log(`Логін користувача: ${user.getLogin()}`);
